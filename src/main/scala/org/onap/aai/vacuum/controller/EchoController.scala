package org.onap.aai.vacuum.controller

import org.springframework.web.bind.annotation.RequestMethod.GET
import org.springframework.web.bind.annotation.{RestController, RequestMapping}

@RestController
class EchoController {
  @RequestMapping(path = Array("/echo"))
  def echo(): Unit = {}
}
