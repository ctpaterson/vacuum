package org.onap.aai.vacuum

import java.util.Scanner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class VacuumApp

object VacuumApp extends App {
  SpringApplication.run(classOf[VacuumApp])
}

