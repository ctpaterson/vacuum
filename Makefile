default:
	cat ./Makefile
jar:
	mvn clean package
image:
	docker build -t vacuum:latest .
run:
	docker run -p 9599:9599 --name vacuum vacuum:latest
