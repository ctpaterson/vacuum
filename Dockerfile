FROM ubuntu:20.04
MAINTAINER C.T. Paterson <ctpaterson+onap@gmail.com>

RUN apt-get update

#configure the JDK 
RUN apt-get install -y openjdk-11-jdk
ENV JAVA_HOME /usr/lib/jvm/java-11-openjdk-amd64
ENV PATH $PATH:/usr/lib/jvm/java-11-openjdk-amd64/jre/bin:/usr/lib/jvm/java-11-openjdk-amd64/bin
ENV CLASSPATH .:${JAVA_HOME}/lib:${JRE_HOME}/lib
ENV JRE_HOME ${JAVA_HOME}/jre

# install vacuum
RUN groupadd -r app && useradd -r -gapp app
RUN mkdir -m 0755 -p /opt/app/vacuum/bin
RUN mkdir -m 0755 -p /opt/app/vacuum/config
RUN mkdir -m 0755 -p /opt/app/vacuum/config/layouts
RUN mkdir -m 0755 -p /opt/app/vacuum/logs/

COPY target/vacuum.jar /opt/app/vacuum/bin
COPY docker-entrypoint.sh /opt/app/vacuum/bin
COPY src/main/resources/application.properties /opt/app/vacuum/config

RUN chown -R app:app /opt/app
RUN chmod +x /opt/app/vacuum/bin/docker-entrypoint.sh

EXPOSE 9599

CMD ["/opt/app/vacuum/bin/docker-entrypoint.sh"]

