# Vacuum

## Overview
Vacuum is ...

## External Dependency


## Running The App
```
make jar
java -jar target/vacuum.jar
```

## Running The App In Docker

```
make jar
make image
make run
```


## Testing The Endpoints
Point your browser to `http://localhost:9599/echo` or use `curl` in command line.

```
curl -v  http://localhost:9599/echo
```

## Makefile
A wrapper Makefile can save some keystrokes. Type `make dist image run` to build and run the app in Docker.

More to come.
